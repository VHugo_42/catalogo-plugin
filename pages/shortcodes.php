<?php
	/*
		Retorna lista com os post types armazenados em base de dados ordenado pela data da criação
		Parâmetros suportados:
			type - nome do custom post type a ser listado (requerido)
			label_post - nome da coluna com os nomes dos posts (opcional)
			label_author - nome da coluna com os nomes dos autores dos posts (opcional)
		shotcode mínimo: [ListaPostType type="nome_post_type"]
		shortcode completo: [ListaPostType type="nome_post_type" label_post="nome_coluna_posts" label_author="nome_coluna_autor"]
	*/
	function listPostTypeByName($atts){
		global $wpdb;//interação com a base de dados
		
		if(isset($atts['type'])){//se o shortcode foi chamado corretamente
			$query = "SELECT $wpdb->posts.ID, $wpdb->posts.post_title, $wpdb->users.user_login
					  FROM $wpdb->posts, $wpdb->users
					  WHERE $wpdb->posts.post_type = '".$atts['type']."'
					  	AND $wpdb->posts.post_status = 'publish'
					  	AND $wpdb->posts.post_author = $wpdb->users.ID
					    ORDER BY $wpdb->posts.post_date DESC";//natural join entre posts e user ordenado pela data					    
			$results = $wpdb->get_results($query);//executa query

			//geração da tabela com os dados retornados
			$table = "<table class='table_data'>";

			if(isset($atts['label_post'])) $table .= "<th>".$atts['label_post']."</th>";
			else $table .= "<th>Post</th>";//nome padrão da coluna
			
			if(isset($atts['label_author'])) $table .= "<th>".$atts['label_author']."</th>";
			else $table .= "<th>Author</th>";//nome padrão da coluna
			
			foreach($results as $result){
				$table .= "<tr>
				<td><a href='".get_permalink($result->ID)."'>$result->post_title</a></td>
				<td>$result->user_login</td>
				</tr>";
			}
			$table .= "</table>";

			//return $query;
			return $table;
		}
		else return "Formato mínimo do shortcode: [ListaPostType type='nome_post_type']";
	}
	add_shortcode('ListaPostType', 'listPostTypeByName');

	/*
		Retorna lista com os dados cadastrados em um gravity forms pelo seu ID
		Parâmetros suportados:
			formid - ID do form a se resgadar dados
		shotcode mínimo: [ListaDadosGravity formid='id_form']
	*/
	function gravityTableData($atts){
		global $wpdb;//interação com a base de dados

		if(isset($atts['formid'])){//se o shortcode foi chamado corretamente
			$query = "SELECT max(field_number) AS last
				FROM ".$wpdb->prefix."rg_lead_detail
				WHERE form_id = ".$atts['formid'];//retorna o número da última coluna do formId
			$last = $wpdb->get_col($query);//retorna apenas uma coluna

			$query = "SELECT field_number, value
				FROM ".$wpdb->prefix."rg_lead_detail
				WHERE form_id = ".$atts['formid'];//retorna os dados do formId
			$results = $wpdb->get_results($query);//executa query

			//geração da tabela com os dados retornados
			$table = "<table class='table_data'>";
			foreach($results as $result){
				if($result->field_number == 1) $table .= "<tr>";//se for a primeira coluna, abrir linha
				$table .= "<td>$result->value</td>";//adição de coluna com o dado em si
				if($result->field_number == $last) $table .= "</tr>";//se for a última coluna, fechar linha
			}
			$table .= "</table>";

			//return $query;
			return $table;
		}
		else return "Formato mínimo do shortcode: [ListaDadosGravity formid='id_form']";
	}
	add_shortcode('ListaDadosGravity', 'gravityTableData');
?>