<?php
	/*
		Plugin Name: Catalogo
		Plugin URI: test.com
		Description: Acidiona os CPT e taxonomias DVDs, CDs e Livros; acidiona um shortcode de listagem dos CPTs
		Author: Victor
		Version: 1.0.0
		Author URI: vctsoft.github.io
		License: GPL2 or Later
	*/

	//inclusão dos arquivos de criação de custom post types e taxonomias
	include "includes/custom_post_dvds.php";
	include "includes/custom_post_cds.php";
	include "includes/custom_post_livros.php";

	include "pages/shortcodes.php";//arquivo de adição de funções-shortcode

	//adição do css utilizado pelo plugin
	function addCSS(){
		if(!is_admin()){
			wp_enqueue_style('frontend', plugins_url('css/frontend.css', __FILE__));
		}
	}
	add_action('init', 'addCSS');
?>