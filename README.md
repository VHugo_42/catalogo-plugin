#Catalogo Plugin

Adiciona:

* Custom Post Types
    * DVD
    * CD
    * Livros
* Shortcodes
    * ListaPostType
        * mínimo: [ListaPostType type="nome_post_type"]
        * completo [ListaPostType type="nome_post_type" label_post="nome_coluna_posts" label_author="nome_coluna_autor"]
    * ListaDadosGravity
        * mínimo: [ListaDadosGravity formid='id_form']