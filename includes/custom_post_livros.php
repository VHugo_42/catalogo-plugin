<?php
	// Custom post type
	function registra_livros() {
		$labels = array(
			'name'               => 'Livros',
			'singular_name'      => 'Livro',
			'menu_name'          => 'Livros',
			'name_admin_bar'     => 'Livro',
			'add_new'            => 'Novo Livro',
			'add_new_item'       => 'Novo Livro',
			'new_item'           => 'Novo Livro',
			'edit_item'          => 'Editar Livro',
			'view_item'          => 'Visualizar Livro',
			'all_items'          => 'Todos Livros',
			'search_items'       => 'Encontrar',
			'parent_item_colon'  => 'Pais:',
			'not_found'          => 'Nada encontrado.',
			'not_found_in_trash' => 'Nada encontrado.',
		);
		 
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'rewrite' => array('slug' => 'livros'),
			'can_export' => true,
			'taxonomies' => array('post_tag'),
			'supports' => array(
				'title', 
				'editor', 
				'author', 
				'thumbnail',  
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				//'page-attributes',
				'post-formats'
			),
		);
		 
		// Registra o custom post tutsup_filmes register_post_type($nome_post_type, $args)
		register_post_type( 'livros', $args );
		flush_rewrite_rules();//correção erro 404
		
		// Registra a categoria personalizada
		
		register_taxonomy( 
			'autor', 
			array( 
				'livros' 
			),
			array(
				'hierarchical' => true,
				'label' => 'Autor',
				'show_ui' => true,
				'show_in_tag_cloud' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'livro_autor'),
			)
		);

		register_taxonomy( 
			'editora', 
			array( 
				'livros' 
			),
			array(
				'hierarchical' => true,
				'label' => 'Editora',
				'show_ui' => true,
				'show_in_tag_cloud' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'livro_editora'),
			)
		);
	}
	// Adiciona a ação
	add_action('init', 'registra_livros');

	// Adiciona o custom posts na query principal
	/*function add_my_post_types_to_query( $query ) {
		if ( $query->is_main_query() && is_home() ) {
			$query->set('post_type', array( 'post', 'cds' ));
			return $query;
		}
	}
	add_action('pre_get_posts', 'add_my_post_types_to_query');*/
?>