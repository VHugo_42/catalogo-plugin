<?php
	// Custom post type
	function registra_cds() {
		$labels = array(
			'name'               => 'CDs',
			'singular_name'      => 'Cd',
			'menu_name'          => 'CDs',
			'name_admin_bar'     => 'Cd',
			'add_new'            => 'Novo CD',
			'add_new_item'       => 'Novo CD',
			'new_item'           => 'Novo CD',
			'edit_item'          => 'Editar CD',
			'view_item'          => 'Visualizar CD',
			'all_items'          => 'Todos CDs',
			'search_items'       => 'Encontrar',
			'parent_item_colon'  => 'Pais:',
			'not_found'          => 'Nada encontrado.',
			'not_found_in_trash' => 'Nada encontrado.',
		);
		 
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'rewrite' => array('slug' => 'cds'),
			'can_export' => true,
			'taxonomies' => array('post_tag'),
			'supports' => array(
				'title', 
				'editor', 
				'author', 
				'thumbnail',  
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				//'page-attributes',
				'post-formats'
			),
		);
		 
		// Registra o custom post tutsup_filmes register_post_type($nome_post_type, $args)
		register_post_type( 'cds', $args );
		flush_rewrite_rules();//correção erro 404
		
		// Registra a categoria personalizada
		
		register_taxonomy( 
			'artista', 
			array( 
				'cds' 
			),
			array(
				'hierarchical' => true,
				'label' => 'Artista',
				'show_ui' => true,
				'show_in_tag_cloud' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'cd_genero'),
			)
		);
	}
	// Adiciona a ação
	add_action('init', 'registra_cds');

	// Adiciona o custom posts na query principal
	/*function add_my_post_types_to_query( $query ) {
		if ( $query->is_main_query() && is_home() ) {
			$query->set('post_type', array( 'post', 'cds' ));
			return $query;
		}
	}
	add_action('pre_get_posts', 'add_my_post_types_to_query');*/
?>