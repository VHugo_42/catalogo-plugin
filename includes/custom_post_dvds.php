<?php
	// Custom post type
	function registra_filmes() {
		$labels = array(
			'name'               => 'DVDs',
			'singular_name'      => 'Dvd',
			'menu_name'          => 'DVDs',
			'name_admin_bar'     => 'Dvd',
			'add_new'            => 'Novo DVD',
			'add_new_item'       => 'Novo DVD',
			'new_item'           => 'Novo DVD',
			'edit_item'          => 'Editar DVD',
			'view_item'          => 'Visualizar DVD',
			'all_items'          => 'Todos DVDs',
			'search_items'       => 'Encontrar',
			'parent_item_colon'  => 'Pais:',
			'not_found'          => 'Nada encontrado.',
			'not_found_in_trash' => 'Nada encontrado.',
		);
		 
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'rewrite' => array('slug' => 'dvds'),
			'can_export' => true,
			'taxonomies' => array('post_tag'),
			'supports' => array(
				'title', 
				'editor', 
				'author', 
				'thumbnail',  
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				//'page-attributes',
				'post-formats'
			),
		);
		 
		// Registra o custom post tutsup_filmes register_post_type($nome_post_type, $args)
		register_post_type( 'dvds', $args );
		flush_rewrite_rules();//correção erro 404
		
		// Registra a categoria personalizada
		
		register_taxonomy( 
			'genero', 
			array( 
				'dvds' 
			),
			array(
				'hierarchical' => true,
				'label' => 'Genero',
				'show_ui' => true,
				'show_in_tag_cloud' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'dvd_genero'),
			)
		);

		register_taxonomy( 
			'diretor', 
			array( 
				'dvds' 
			),
			array(
				'hierarchical' => true,
				'label' => 'Diretor',
				'show_ui' => true,
				'show_in_tag_cloud' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'dvd_diretor'),
			)
		);
	}
	// Adiciona a ação
	add_action('init', 'registra_filmes');

	// Adiciona o custom posts na query principal
	/*function add_my_post_types_to_query( $query ) {
		if ( $query->is_main_query() && is_home() ) {
			$query->set( 'post_type', array( 'post', 'filmes' ) );
			return $query;
		}
	}
	add_action('pre_get_posts', 'add_my_post_types_to_query');*/
?>